---Security---
SSL not required for login
Common Admin username

Settings file insecure

Users are not allowed to input dangerous HTML tags

Authenticated users do not have admin privileges

Anonymous users do not have admin privileges

Unsafe file extensions are not allowed in uploads

Ten or less website admins

PHP filter module disabled
