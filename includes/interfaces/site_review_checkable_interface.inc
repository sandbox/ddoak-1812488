<?php
/**
 * @file
 * Defines checkable behavior.
 *
 * @author Alan Moreau <almor@smile.fr>
 */

interface SiteReviewCheckableInterface {

  /**
   * Set parameter name.
   *
   * @param string $name
   *   The parameter name
   */
  public function setName($name);

  /**
   * Set expected value of parameter.
   *
   * @param string $value
   *   The parameter expected value
   */
  public function setExpectedValue($value);

  /**
   * Set options.
   *
   * @param array $options
   *   An associative array containing extra options to pass to the checker
   */
  public function setOptions(array $options);

  /**
   * Set success message.
   *
   * @param string $message
   *   The success message
   */
  public function setSuccessMessage($message);

  /**
   * Set failed message.
   *
   * @param string $message
   *   The fail message
   */
  public function setFailMessage($message);

  /**
   * Set severity.
   *
   * @param integer $severity
   *   The severity of the failure
   */
  public function setSeverity($severity);

  /**
   * Set an help message.
   *
   * @param string $help
   *   An helping message to guide user to fix the failure
   */
  public function setHelp($help);

  /**
   * Retrieve parameter name.
   *
   * @return string
   *   The parameter name
   */
  public function getName();

  /**
   * Retrieve expected value of parameter.
   *
   * @return string
   *   The parameter expected value
   */
  public function getExpectedValue();

  /**
   * Retrieve options.
   *
   * @return array
   *   An associative array containing extra options to pass to the checker
   */
  public function getOptions();

  /**
   * Retrieve success message.
   *
   * @return string
   *   success message
   */
  public function getSuccessMessage();

  /**
   * Retrieve fail message.
   *
   * @return string
   *   fail message
   */
  public function getFailMessage();

  /**
   * Retrieve severity.
   *
   * @return integer
   *   severity
   */
  public function getSeverity();

  /**
   * Get help message.
   *
   * @return string
   *   The help string
   */
  public function getHelp();

}
