<?php
/**
 * @file
 * Defines report generation behavior.
 *
 * @author Alan Moreau <almor@smile.fr>
 */

interface SiteReviewReportInterface {

  /**
   * Add a test result.
   *
   * @param string $category
   *   The test category
   * @param string $test_id
   *   The test identifier
   */
  public function addResult($category, $test_id, SiteReviewCheckerResult $result);

  /**
   * Return a rendered report.
   *
   * @return string
   *   The report to print
   */
  public function getReport();

}
