<?php
/**
 * @file
 * Defines a checker behavior.
 *
 * @author Alan Moreau <almor@smile.fr>
 */

interface SiteReviewCheckerInterface {

  /**
   * Run the chcking process.
   */
  public function check();


  /**
   * Return the test result.
   *
   * @return SiteReviewCheckerResult
   *   Checker result
   */
  public function result();

}
