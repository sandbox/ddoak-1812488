<?php
/**
 * @file
 * Defines a test result.
 *
 * @author Alan Moreau <almor@smile.fr>
 */

class SiteReviewCheckerResult {

  /**
   * Severity error level
   * @var integer
   */
  const ERROR = 2;

  /**
   * Severity warning level
   * @var integer
   */
  const WARNING = 1;

  /**
   * Test label
   * @var string
   */
  protected $label;

  /**
   * Test status
   * @var boolean
   */
  protected $status;

  /**
   * Test severity
   * @var integer
   */
  protected $severity;

  /**
   * Test result message
   * @var string
   */
  protected $message;

  /**
   * Help
   * @var string
   */
  protected $help;

  /**
   * Intialize test result properties.
   *
   * @param string $label
   *   The test label
   * @param boolean $status
   *   The test status
   * @param integer $severity
   *   The severity of a failed status
   * @param string $message
   *   The test status message
   * @param string $help
   *   The help message
   */
  public function __construct($label, $status, $severity, $message, $help) {
    $this->label = $label;
    $this->message = $message;
    $this->status = $status;
    $this->help = $help;
    $this->severity = $severity;
  }

  /**
   * Retrieve test result label.
   *
   * @return string
   *   Label
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Retrieve test result status.
   *
   * @return boolean
   *   Status
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Retrieve test result severity.
   *
   * @return integer
   *   Severity
   */
  public function getSeverity() {
    return $this->severity;
  }

  /**
   * Retrieve test result message.
   *
   * @return string
   *   Message
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * Retrieve help message.
   *
   * @return string
   *   Help
   */
  public function getHelp() {
    return $this->help;
  }
}
