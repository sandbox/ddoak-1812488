<?php
/**
 * @file
 * SiteReviewCheckerFactory provides checker instances.
 */

/**
 * SiteReviewCheckerFactory provides checker instances.
 *
 * @author Alan Moreau <almor@smile.fr>
 */
class SiteReviewCheckerFactory {

  /**
   * Stores registered checkers
   * @var array<SiteReviewCheckerInterface>
   */
  protected $checkers = array();

  /**
   * SiteReviewCheckerFactory single instance
   * @var SiteReviewTestFactory
   */
  private static $instance = NULL;

  /**
   * Private constructor to protect factory against multiple instances registers
   * native checkers.
   */
  private function __construct() {
    $this->register('drupal_variable', new SiteReviewDrupalVariableChecker());
    $this->register('drupal_module', new SiteReviewDrupalModuleChecker());
    $this->register('php_extension', new SiteReviewPhpExtensionChecker());
    $this->register('php_configuration', new SiteReviewPhpConfigurationChecker()
      );
  }

  /**
   * Retrieves SiteReviewCheckerFactory single instance.
   *
   * @return SiteReviewCheckerFactory
   *   Instance
   */
  public static function getInstance() {
    if (self::$instance === NULL) {
      self::$instance = new SiteReviewCheckerFactory();
    }
    return self::$instance;
  }

  /**
   * Retrieves an instance of given checker.
   *
   * @param string $checker_id
   *   Checker id
   *
   * @return SiteReviewCheckerInterface
   *   Checker instance or NULL for unkown id
   */
  public function getCheckerFor($checker_id) {
    $checker = NULL;
    if (isset($this->checkers[$checker_id])) {
      $checker = $this->checkers[$checker_id];
    }
    elseif (class_exists($checker_id)) {
      $class = new $checker_id();
      if (in_array('SiteReviewCheckerInterface', class_implements($class))) {
        $checker = $class;
      }
    }

    return $checker;
  }

  /**
   * Register checker SiteReviewCheckerInterface.
   *
   * @param string $key
   *   The checker identifier
   * @param SiteReviewCheckerInterface $checker
   *   Checker instance to register
   */
  public function register($key, SiteReviewCheckerInterface $checker) {
    $this->checkers[$key] = $checker;
  }

}
