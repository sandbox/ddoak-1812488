<?php
/**
 * @file
 * Defines SiteReviewCheckerParams.
 */

/**
 * SiteReviewCheckerParams is an implementation of SiteReviewCheckableInterface.
 * It provides an easy way to check a drupal param or php settings through a the
 * right checker.
 *
 * @see SiteReviewDrupalModuleChecker
 * @see SiteReviewDrupalVariableChecker
 * @see SiteReviewPhpConfigurationChecker
 * @see SiteReviewPhpExtensionChecker
 */
class SiteReviewCheckerParams implements SiteReviewCheckableInterface {

  /**
   * Parameter name
   * @var string
   */
  protected $paramName;

  /**
   * Parameter expected value
   * @var string
   */
  protected $paramExpectedValue;

  /**
   * Extra options
   * @var array
   */
  protected $options;

  /**
   * Fail message
   * @var string
   */
  protected $failedMessage;

  /**
   * Success message
   * @var string
   */
  protected $successMessage;

  /**
   * Severity level
   * @var integer
   */
  protected $severity;

  /**
   * Help message
   * @var string
   */
  protected $help;

  /**
   * Create new instance of SiteReviewCheckerParams.
   *
   * @param string $param_name
   *   The param to check
   * @param mixed $param_expected_value
   *   Expected value of param
   * @param string $failed_message
   *   The failed message
   * @param string $success_message
   *   The success message
   * @param string $help
   *   The help message
   * @param integer $severity
   *   The severity of the failed status
   * @param array $options
   *   Extra options to pass to the checker
   */
  public function __construct($param_name, $param_expected_value, $failed_message,
    $success_message, $help = '', $severity = 2, array $options = array()) {

    $this->paramName = $param_name;
    $this->paramExpectedValue = $param_expected_value;
    $this->options = $options;
    $this->successMessage = $success_message;
    $this->failedMessage = $failed_message;
    $this->severity = $severity;
    $this->help = $help;
  }

  /**
   * Set parameter name.
   */
  public function setName($name) {
    $this->paramName = $name;
  }

  /**
   * Set expected value of parameter.
   */
  public function setExpectedValue($value) {
    $this->paramExpectedValue = $value;
  }

  /**
   * Set options.
   */
  public function setOptions(array $options) {
    $this->options = $options;
  }

  /**
   * Set success message.
   */
  public function setSuccessMessage($message) {
    $this->successMessage = $message;
  }

  /**
   * Set failed message.
   */
  public function setFailMessage($message) {
    $this->failedMessage = $message;
  }

  /**
   * Set severity.
   */
  public function setSeverity($severity) {
    $this->severity = $severity;
  }

  /**
   * Set help.
   */
  public function setHelp($help) {
    $this->help = $help;
  }

  /**
   * Retrieve parameter name.
   */
  public function getName() {
    return $this->paramName;
  }

  /**
   * Retrieve expected value of parameter.
   */
  public function getExpectedValue() {
    return $this->paramExpectedValue;
  }

  /**
   * Get options.
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * Retrieve success message.
   */
  public function getSuccessMessage() {
    return $this->successMessage;
  }

  /**
   * Retrieve failed message.
   */
  public function getFailMessage() {
    return $this->failedMessage;
  }

  /**
   * Retrieve severity.
   */
  public function getSeverity() {
    return $this->severity;
  }

  /**
   * Get help.
   */
  public function getHelp() {
    return $this->help;
  }

}
