<?php
/**
 * @file
 * Defines CLI report generation.
 */

/**
 * Defines CLI report generation.
 *
 * @author Alan Moreau <almor@smile.fr>
 */
class SiteReviewCliReport implements SiteReviewReportInterface {

  /**
   * Define CLI Red color
   * @var string
   */
  const CLI_RED_COLOR = "\033[0;31m";

  /**
   * Define CLI Green color
   * @var string
   */
  const CLI_GREEN_COLOR = "\033[0;32m";

  /**
   * Define CLI Yellow color
   * @var string
   */
  const CLI_YELLOW_COLOR = "\033[1;33m";

  /**
   * Define CLI Neutral color
   * @var string
   */
  const CLI_NEUTRAL_COLOR = "\033[1;37m";

  /**
   * Tests result
   * @var array
   */
  protected $data = NULL;

  /**
   * Push a test result in the report.
   *
   * @param string $category
   *   The name of the group tests
   * @param string $test_id
   *   The test machine name
   * @param SiteReviewCheckerResult $result
   *   The test result
   */
  public function addResult($category, $test_id, SiteReviewCheckerResult $result) {
    $this->data[$category][$test_id] = $result;
  }

  /**
   * Return a rendered report.
   *
   * @return string
   *   Rendered report
   */
  public function getReport() {
    $rows = array(array(SiteReviewCliReport::CLI_NEUTRAL_COLOR, '', ''));

    foreach ($this->data as $name => $report_suite) {
      $rows_group = array(array($name, '', ''));
      foreach ($report_suite as $test_id => $result) {
        $rows_group[] = $this->getRow($result);
      }
      $rows = array_merge($rows, $rows_group);
    }

    return $rows;
  }

  /**
   * Get a formatted row ready to be displayed.
   *
   * @param SiteReviewCheckerResult $result
   *   Test result
   * @param boolean $is_colored
   *   Is the result must be display with appropriated color
   *
   * @return array
   *   The row to display.
   */
  protected function getRow(SiteReviewCheckerResult $result, $is_colored = TRUE) {
    $label = $result->getLabel();
    $message = $result->getMessage();
    $status = $result->getStatus();
    $color = '';
    $status_label = '';
    if ($status) {
      $status_label = t('Success');
      $color = SiteReviewCliReport::CLI_GREEN_COLOR;
    }
    else {
      $severity = $result->getSeverity();
      switch ($severity) {

        case 1:
          $status_label = t('Warning');
          $color = SiteReviewCliReport::CLI_YELLOW_COLOR;
          break;

        default:
          $status_label = t('Failed');
          $color = SiteReviewCliReport::CLI_RED_COLOR;
          break;

      }
    }

    if ($is_colored) {
      $status_label = $color . ' [' . $status_label . '] ' . SiteReviewCliReport::CLI_NEUTRAL_COLOR;
    }

    return array(
      $label,
      $message,
      $status_label,
    );
  }
}
