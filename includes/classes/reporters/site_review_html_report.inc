<?php
/**
 * @file
 * Defines HTML report generation.
 */

/**
 * Defines HTML report generation.
 *
 * @author Alan Moreau <almor@smile.fr>
 */
class SiteReviewHtmlReport implements SiteReviewReportInterface {

  /**
   * Tests result
   * @var array
   */
  protected $data = NULL;

  /**
   * Push a test result in the report.
   *
   * @param string $category
   *   The name of the group tests
   * @param string $test_id
   *   The test machine name
   * @param SiteReviewCheckerResult $result
   *   The test result
   */
  public function addResult($category, $test_id, SiteReviewCheckerResult $result) {
    $this->data[$category][$test_id] = $result;
  }

  /**
   * Return a rendered report.
   *
   * @return string
   *   Rendered report
   */
  public function getReport() {
    $suites = array();
    $header = array(t('STATUS'), t('LABEL'), t('MESSAGE'));
    foreach ($this->data as $name => $report_suite) {
      $rows = array('', $name, '');
      foreach ($report_suite as $report_entry) {
        $rows[] = $this->getRow($report_entry);
      }
      $suites[$name] = theme('table', array('header' => $header, 'rows' => $rows));
    }

    drupal_add_library('system', 'ui.tabs');
    drupal_add_js(drupal_get_path('module', 'site_review') . '/js/site_review.js');

    return theme('site_review_html_report', array('suites' => $suites));
  }

  /**
   * Get a formatted row ready to be displayed.
   *
   * @param SiteReviewCheckerResult $result
   *   Test result
   *
   * @return array
   *   The row to display.
   */
  protected function getRow(SiteReviewCheckerResult $result) {
    $label = $result->getLabel();
    $message = $result->getMessage();
    $status = $result->getStatus();

    $path = 'misc';
    $alt = '';
    if ($status) {
      $path .= '/message-24-ok.png';
      $alt = t('Success');
    }
    else {
      $severity = $result->getSeverity();
      switch ($severity) {

        case 1:
          $path .= '/message-24-warning.png';
          $alt = t('Warning');
          break;

        default:
          $path .= '/message-24-error.png';
          $alt = t('Failed');
          break;

      }
    }
    $status_icon = theme('image', array('path' => $path, 'alt' => $alt));

    return array(
      $status_icon,
      $label,
      $message,
    );
  }

  /**
   * Return theme declaration.
   *
   * @return array
   *   Theme declaration.
   */
  public static function getTheme($path) {
    return array(
      'site_review_html_report' => array(
        'variables' => array(
          'suites' => NULL,
        ),
        'path' => $path . '/templates',
        'template' => 'site-review-report',
      ),
    );
  }
}
