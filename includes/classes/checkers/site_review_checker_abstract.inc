<?php
/**
 * @file
 * SiteReviewCheckerAbstract provides an abstract implementation of checkers
 * using SiteReviewCheckerParams.
 */

/**
 * SiteReviewCheckerAbstract provides an abstract implementation of checkers
 * using SiteReviewCheckerParams.
 *
 * @author Alan Moreau <almor@smile.fr>
 */
abstract class SiteReviewCheckerAbstract implements SiteReviewCheckerInterface {

  /**
   * Define "equal" operator
   * @var string
   */
  const EQUALS_OP = '=';

  /**
   * Define "lower than" operator
   * @var string
   */
  const LOWER_THAN_OP = '<';

  /**
   * Define "greater than" operator
   * @var string
   */
  const GREATER_THAN_OP = '>';

  /**
   * Test result instance
   * @var SiteReviewCheckerResult
   */
  protected $result = NULL;

  /**
   * Test params instance
   * @var SiteReviewCheckerParams
   */
  protected $params = NULL;

  /**
   * Test label
   * @var string
   */
  protected $label = NULL;

  /**
   * Set params.
   *
   * @param string $label
   *   the checker label
   * @param SiteReviewCheckerParams $input
   *   the checker input params
   */
  public function setParams($label, SiteReviewCheckerParams $input) {
    $this->label = $label;
    $this->params = $input;
  }

  /**
   * Return Test result.
   *
   * @return SiteReviewCheckerResult
   *   The test result
   */
  public function result() {
    return $this->result;
  }

  /**
   * Create test result instance.
   *
   * @param string $label
   *   The checker label
   * @param boolean $status
   *   The test status
   * @param integer $severity
   *   The test severity
   * @param string $message
   *   The test message
   * @param string $help
   *   The test help
   */
  protected function createResult($label, $status, $severity, $message, $help) {
    $this->result = new SiteReviewCheckerResult($label, $status, $severity,
      $message, $help);
  }
}
