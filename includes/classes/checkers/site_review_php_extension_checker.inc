<?php
/**
 * @file
 * SiteReviewPhpExtensionChecker provides a way to check a php extension status.
 */

/**
 * SiteReviewPhpExtensionChecker provides a way to check a php extension status.
 *
 * @author Alan Moreau <almor@smile.fr>
 */
class SiteReviewPhpExtensionChecker extends SiteReviewCheckerAbstract {

  /**
   * Implementation of check method.
   */
  public function check() {
    $severity = $this->params->getSeverity();
    $message  = $this->params->getFailMessage();
    $help     = $this->params->getHelp();
    $status   = FALSE;

    $name           = $this->params->getName();
    $expected_value = $this->params->getExpectedValue();

    if ($expected_value === extension_loaded($name)) {
      $status  = TRUE;
      $message = $this->params->getSuccessMessage();
    }

    $this->createResult($this->label, $status, $severity, $message, $help);
  }

}
