<?php
/**
 * @file
 * SiteReviewDrupalVariableChecker provides a way to check drupal variable
 * value.
 */

/**
 * SiteReviewDrupalVariableChecker provides a way to check drupal variable
 * value.
 *
 * @author Alan Moreau <almor@smile.fr>
 */
class SiteReviewDrupalVariableChecker extends SiteReviewCheckerAbstract {

  /**
   * Implementation of check method.
   */
  public function check() {
    $severity = $this->params->getSeverity();
    $message  = $this->params->getFailMessage();
    $help     = $this->params->getHelp();
    $status   = FALSE;

    $var_name       = $this->params->getName();
    $expected_value = $this->params->getExpectedValue();

    $value = variable_get($var_name, '');

    $operator = self::EQUALS_OP;
    $options = $this->params->getOptions();
    if (isset($options['operator'])) {
      $operator = $options['operator'];
    }

    switch ($operator) {

      case self::EQUALS_OP:
        $status = $value == $expected_value;
        break;

      case self::LOWER_THAN_OP:
        $status = $value < $expected_value;
        break;

      case self::GREATER_THAN_OP:
        $status = $value > $expected_value;
        break;

      default:
        // TODO throw invalid operator exception.
        break;

    }

    if ($status) {
      $message = $this->params->getSuccessMessage();
    }

    $this->createResult($this->label, $status, $severity, $message, $help);
  }
}
