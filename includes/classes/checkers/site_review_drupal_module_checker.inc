<?php
/**
 * @file
 * SiteReviewDrupalModuleChecker provides a way to check drupal module status.
 */

/**
 * SiteReviewDrupalModuleChecker provides a way to check drupal module status.
 *
 * @author Alan Moreau <almor@smile.fr>
 */
class SiteReviewDrupalModuleChecker extends SiteReviewCheckerAbstract {

  /**
   * Implementation of check method.
   */
  public function check() {
    $severity = $this->params->getSeverity();
    $message  = $this->params->getFailMessage();
    $help     = $this->params->getHelp();
    $status   = FALSE;

    $name           = $this->params->getName();
    $expected_value = $this->params->getExpectedValue();

    if ($expected_value === module_exists($name)) {
      $status  = TRUE;
      $message = $this->params->getSuccessMessage();
    }

    $this->createResult($this->label, $status, $severity, $message, $help);
  }

}
