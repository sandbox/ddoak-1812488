<?php
/**
 * @file
 * SiteReviewPhpConfigurationChecker provides a way to check a php setting
 * value.
 */

/**
 * SiteReviewPhpConfigurationChecker provides a way to check a php setting
 * value.
 *
 * @author Alan Moreau <almor@smile.fr>
 */
class SiteReviewPhpConfigurationChecker extends SiteReviewCheckerAbstract {

  /**
   * Implementation of check method.
   */
  public function check() {
    $severity = $this->params->getSeverity();
    $message  = $this->params->getFailMessage();
    $help     = $this->params->getHelp();
    $status   = FALSE;

    $name = $this->params->getName();
    $expected_value = $this->params->getExpectedValue();

    $value = ini_get($name);

    $operator = self::EQUALS_OP;
    $options = $this->params->getOptions();
    if (isset($options['operator'])) {
      $operator = $options['operator'];
    }

    switch ($operator) {

      case self::EQUALS_OP:
        $status = $value == $expected_value;
        break;

      case self::LOWER_THAN_OP:
        $status = $value < $expected_value;
        break;

      case self::GREATER_THAN_OP:
        $status = $value > $expected_value;
        break;

      default:
        // TODO throw invalid operator exception.
        break;

    }

    if ($status) {
      $message = $this->params->getSuccessMessage();
    }

    $this->createResult($this->label, $status, $severity, $message, $help);
  }

}
