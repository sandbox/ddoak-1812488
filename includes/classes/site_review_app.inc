<?php
/**
 * @file
 * SiteReviewApp launches tests suites.
 */

class SiteReviewApp {

  protected $config = array();

  /**
   * Construct a new instance of SiteReviewApp.
   *
   * @param array $config
   *   An array containing tests suites definition
   * @param SiteReviewReportInterface $reporter
   *   The reporter (Drush, HTML THEME)
   */
  public function __construct(array $config,
    SiteReviewReportInterface $reporter) {

    $this->config = $config;
    $this->reporter = $reporter;

    foreach ($this->config as $name => $checkers_suite) {
      foreach ($checkers_suite as $test_name => $checker) {

        $checker_instance = SiteReviewCheckerFactory::getInstance()
        ->getCheckerFor($checker['type']);

        if ($checker === NULL) {
          // TODO throw new Exception.
        }

        if (isset($checker['params'])) {
          $help = isset($checker['params']['help']) ?
            $checker['params']['help'] : '';

          $severity = isset($checker['params']['severity']) ?
            $checker['params']['severity'] : SiteReviewCheckerResult::ERROR;

          $options = isset($checker['params']['options']) ?
            $checker['params']['options'] : array();

          $params = new SiteReviewCheckerParams(
            $checker['params']['name'],
            $checker['params']['expected_value'],
            $checker['params']['fail_message'],
            $checker['params']['success_message'],
            $help,
            $severity,
            $options
          );
          $checker_instance->setParams($checker['label'], $params);
        }

        $checker_instance->check();

        $this->reporter
          ->addResult($name, $test_name, $checker_instance->result());
      }
    }
  }

  /**
   * Return rendered report according to given reporter.
   *
   * @return string
   *   Rendered report
   */
  public function getResponse() {
    return $this->reporter->getReport();
  }
}
