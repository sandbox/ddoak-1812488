(function ($) {
  Drupal.behaviors.site_review = {
    attach: function (context, settings) {
      $('#site-review-tabs').tabs();
    }
  };
})(jQuery);
