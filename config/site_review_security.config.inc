<?php
/**
 * @file
 * Defines security test suite.
 */

/**
 * Defines security test suite.
 *
 * @return array
 *   Tests suite configuration.
 */
function site_review_security_config() {
  $config = array(
    'Security' => array(
      'devel' => array(
        'label' => t('Devel module'),
        'type' => 'drupal_module',
        'params' => array(
          'name' => 'devel',
          'expected_value' => FALSE,
          'fail_message' => t('Devel module enabled'),
          'success_message' => t('Devel module disabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'syslog' => array(
        'label' => t('Syslog module'),
        'type' => 'drupal_module',
        'params' => array(
          'name' => 'syslog',
          'expected_value' => TRUE,
          'fail_message' => t('Syslog module disabled'),
          'success_message' => t('Syslog module enabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'site_errors' => array(
        'label' => t('Site errors visible'),
        'type' => 'drupal_variable',
        'params' => array(
          'name' => 'error_level',
          'expected_value' => 0,
          'fail_message' => t('Site errors visible'),
          'success_message' => t('Site errors display off'),
          'help' => t('To fix it...'),
        ),
      ),
      'php_error_visible' => array(
        'label' => t('PHP errors'),
        'type' => 'php_configuration',
        'params' => array(
          'name' => 'display_errors',
          'expected_value' => 0,
          'fail_message' => t('PHP errors visible'),
          'success_message' => t('PHP display errors off'),
          'help' => t('To fix it...'),
        ),
      ),
      'php_error_logging' => array(
        'label' => t('PHP error logging'),
        'type' => 'php_configuration',
        'params' => array(
          'name' => 'log_errors',
          'expected_value' => 1,
          'fail_message' => t('PHP error logging disabled'),
          'success_message' => t('PHP error logging enabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'register_globals' => array(
        'label' => t('PHP Register Globals'),
        'type' => 'php_configuration',
        'params' => array(
          'name' => 'register_globals',
          'expected_value' => 0,
          'fail_message' => t('PHP Register Globals enabled'),
          'success_message' => t('PHP Register Globals disabled'),
          'help' => t('To fix it...'),
        ),
      ),
    ),
  );

  return $config;
}
