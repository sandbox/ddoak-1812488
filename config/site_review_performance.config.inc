<?php
/**
 * @file
 * Defines performance test suite.
 */

/**
 * Defines performance test suite.
 *
 * @return array
 *   Tests suite configuration.
 */
function site_review_performance_config() {
  $config = array(
    'Performance' => array(
      'dblog' => array(
        'label' => t('Database logging'),
        'type' => 'drupal_module',
        'params' => array(
          'name' => 'dblog',
          'expected_value' => FALSE,
          'fail_message' => t('Database logging module enabled'),
          'success_message' => t('Database logging module disabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'devel' => array(
        'label' => t('Devel module'),
        'type' => 'drupal_module',
        'params' => array(
          'name' => 'devel',
          'expected_value' => FALSE,
          'fail_message' => t('Devel module enabled'),
          'success_message' => t('Devel module disabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'devel_themer' => array(
        'label' => t('Theme developper module'),
        'type' => 'drupal_module',
        'params' => array(
          'name' => 'devel_themer',
          'expected_value' => FALSE,
          'fail_message' => t('Theme developper module enabled'),
          'success_message' => t('Theme developper module disabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'coder' => array(
        'label' => t('Coder module'),
        'type' => 'drupal_module',
        'params' => array(
          'name' => 'coder',
          'expected_value' => FALSE,
          'fail_message' => t('Coder module enabled'),
          'success_message' => t('Coder module disabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'page_caching' => array(
        'label' => t('Page caching'),
        'type' => 'drupal_variable',
        'params' => array(
          'name' => 'cache',
          'expected_value' => 1,
          'fail_message' => t('Page caching disabled'),
          'success_message' => t('Page caching enabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'cache_lifetime' => array(
        'label' => t('Cache lifetime'),
        'type' => 'drupal_variable',
        'params' => array(
          'name' => 'cache_lifetime',
          'expected_value' => 4,
          'options' => array('operator' => SiteReviewCheckerAbstract::GREATER_THAN_OP),
          'fail_message' => t('Minimum cache lifetime less than 5 minutes'),
          'success_message' => t('Minimum cache lifetime more than 5 minutes'),
          'help' => t('To fix it...'),
        ),
      ),
      'page_cache_maximum_age' => array(
        'label' => t('Page cache max age'),
        'type' => 'drupal_variable',
        'params' => array(
          'name' => 'page_cache_maximum_age',
          'expected_value' => 4,
          'options' => array('operator' => SiteReviewCheckerAbstract::GREATER_THAN_OP),
          'fail_message' => t('Page cache max age less than 5 minutes'),
          'success_message' => t('Page cache max age more than 5 minutes'),
          'help' => t('To fix it...'),
        ),
      ),
      'page_compression' => array(
        'label' => t('Page compression'),
        'type' => 'drupal_variable',
        'params' => array(
          'name' => 'page_compression',
          'expected_value' => 1,
          'fail_message' => t('Page compression disabled'),
          'success_message' => t('Page compression enabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'js_optimization' => array(
        'label' => t('JS Optimization'),
        'type' => 'drupal_variable',
        'params' => array(
          'name' => 'preprocess_js',
          'expected_value' => 1,
          'fail_message' => t('JS optimization disabled'),
          'success_message' => t('JS optimization enabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'css_optimization' => array(
        'label' => t('CSS Optimization'),
        'type' => 'drupal_variable',
        'params' => array(
          'name' => 'preprocess_css',
          'expected_value' => 1,
          'fail_message' => t('CSS optimization disabled'),
          'success_message' => t('CSS optimization enabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'theme_rebuild_registry' => array(
        'label' => t('Theme rebuild registry'),
        'type' => 'drupal_variable',
        'params' => array(
          'name' => 'devel_rebuild_theme_registry',
          'expected_value' => FALSE,
          'fail_message' => t('Theme rebuild registry feature enabled'),
          'success_message' => t('Theme rebuild registry feature disabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'php_memcached' => array(
        'label' => t('Memcached PHP extension'),
        'type' => 'php_extension',
        'params' => array(
          'name' => 'memcached',
          'expected_value' => TRUE,
          'fail_message' => t('The memcached PHP extension is not installed'),
          'success_message' => t('The memcached PHP extension is installed'),
          'help' => t('To fix it...'),
        ),
      ),
      'apc' => array(
        'label' => t('APC extension'),
        'type' => 'php_extension',
        'params' => array(
          'name' => 'apc',
          'expected_value' => TRUE,
          'fail_message' => t('PHP APC extension disabled'),
          'success_message' => t('PHP APC extension enabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'php_error_logging' => array(
        'label' => t('PHP error logging'),
        'type' => 'php_configuration',
        'params' => array(
          'name' => 'log_errors',
          'expected_value' => 1,
          'fail_message' => t('PHP error logging disabled'),
          'success_message' => t('PHP error logging enabled'),
          'help' => t('To fix it...'),
        ),
      ),
      'php_max_execution_time' => array(
        'label' => t('PHP maximum execution time'),
        'type' => 'php_configuration',
        'params' => array(
          'name' => 'max_execution_time',
          'expected_value' => 301,
          'options' => array('operator' => SiteReviewCheckerAbstract::LOWER_THAN_OP),
          'fail_message' => t('PHP maximum execution time is up to 300 seconds'),
          'success_message' => t('PHP maximum execution time less than or equals 300 seconds'),
          'help' => t('To fix it...'),
        ),
      ),
    ),
  );

  return $config;
}
