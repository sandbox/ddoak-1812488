<?php

/**
 * @file
 * Drush integration for the Site Review module.
 */

/**
 * Implements hook_drush_command().
 */
function site_review_drush_command() {
  $items['site-review'] = array(
    'description' => 'Launch a site review and print the report',
    'aliases' => array('st'),
  );

  return $items;
}

/**
 * Site review command callback.
 */
function drush_site_review() {
  $config = site_review_config();
  $reporter = new SiteReviewCliReport();
  $app = new SiteReviewApp($config, $reporter);
  $rows = $app->getResponse();
  drush_print_table($rows, FALSE);
}
