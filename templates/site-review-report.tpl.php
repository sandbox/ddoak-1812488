<?php

/**
 * @file
 * Default theme implementation for site review report
 *
 * Available variables:
 * - $suites: An array of results
 */
?>

<?php if ($suites !== NULL): ?>
  <div id="site-review-tabs">
    <ul>
    <?php foreach ($suites as $name => $suite): ?>
      <li>
        <a href="#<?php print $name ?>"><?php print t($name); ?></a>
      </li> 
    <?php endforeach; ?>
    </ul>
    <?php foreach ($suites as $name => $suite): ?>
      <div id="<?php print $name ?>">
        <?php print $suite; ?>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif;?>
