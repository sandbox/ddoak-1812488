
-- ABOUT --

Site Review run tests suites against your Drupal instance.
For now, it checks performance and security settings.

-- INSTALLATION -- 

Install as usual Drupal module

-- USAGE --

Navigate to Administer >> Reports >> Site Review to run the tests suites.

If you use Drush :
$>drush site-review
